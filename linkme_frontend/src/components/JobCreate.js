import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Button from '@material-ui/core/Button';
import MailIcon from '@material-ui/icons/Mail';
import Avatar from '@material-ui/core/Avatar';
import SearchBar from "material-ui-search-bar";
import CategorieList from './employee/CategorieList';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import AppBar from '@material-ui/core/AppBar';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
    backgroundColor:'#ffd100',
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  menuButton: {
    marginRight: 36,
    color:'#000000',
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
    color: '#000000',
    fontFamily: 'Roboto',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  Icon: {
    color: '#000000',
   
  },
  IconAvatar: {
    color: '#ffffff',
    backgroundColor: '#000000',
    height: '25px',
    width: '25px',

  },
  SearchBar: {
    backgroundColor: '#ffd100',
    color: '#000000',
    marginRight: '200px',
  }
 
}));

function jobpost(title,category,duration,location,price,type){
    axios.post("http://localhost:5000/forms/create", {
      title: title,
      category: category,
      // description: description,
      type: type,
      duration: duration,
      location: location,
      price: price,
    }).then(
      (res)=>{
        window.alert("JOB HAS BEEN ADDED!!")
      }
  
    ).catch(
      (err)=>{
         console.log(err)
      }
    )
  }


export default function JobCreate() {

   const classes = useStyles();
   const [open, setOpen] = React.useState(true);

   const [title, getTitle] = useState(0);
   const [category, getCategory] = useState(0);
   const [description, getDescription] = useState(0);
   const [location, getLocation] = useState(0);
   const [duration, getDuration] = useState(0);
   const [price, getPrice] = useState(0);

   const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>  
          <Typography component="h4" variant="h6" color="inherit" noWrap className={classes.title}>
             LINKME
          </Typography>

       <SearchBar className={classes.SearchBar} />
     
       <Button aria-controls="simple-menu" aria-haspopup="true"  onClick={()=>{window.location.href="/find-work"}}> FIND WORK </Button>        
       <Button aria-controls="simple-menu" aria-haspopup="true"  onClick={()=>{window.location.href="/find-work"}}> MY JOBS </Button>
       <Button aria-controls="simple-menu" aria-haspopup="true"  onClick={()=>{window.location.href="/report"}}> REPORTS  </Button>
       <Button aria-controls="simple-menu" aria-haspopup="true"  onClick={()=>{window.location.href="/message"}}> MESSAGES </Button>

          <IconButton color="inherit" className={classes.Icon}>
            <Badge badgeContent={0}>
              <NotificationsIcon />
            </Badge>
          </IconButton>

          <IconButton color="inherit" className={classes.Icon}>
           <Badge badgeContent={2}>
             <MailIcon />
           </Badge>
        
           </IconButton>

           <IconButton>
              <Avatar className={classes.IconAvatar} />
           </IconButton>   
        
        </Toolbar>
      </AppBar>
    
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            {/* Categories */}
            <Grid item xs={12} md={3} lg={3}>
           <CategorieList />
            </Grid>
           
            {/* Jobs */}
            <Grid item xs={12} md={9} lg={9}>
              <Paper item xs={10} md={7} lg={7}>
              <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="Title"
              onChange={(e)=>{
                getTitle(e.target.value)
              }}
              label="Title"
              name="Title"
              autoComplete="Title"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="Category"
              label="Category"
              type="Category"
              id="Category"
              onChange={(e)=>{
                getCategory(e.target.value)
              }}
              autoComplete="Category"
            />
             
             <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="Description"
              label="Description"
              type="Description"
              id="Description"
              onChange={(e)=>{
                getDescription(e.target.value)
              }}
              autoComplete="Description  "
            />
              <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="Type"
              label="Type"
              type="Type"
              id="Type"
              onChange={(e)=>{
                getDescription(e.target.value)
              }}
              autoComplete="Type  "
            />

            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="Duration"
              label="Duration"
              type="Duration"
              id="Duration"
              onChange={(e)=>{
                getDuration(e.target.value)
              }}
              autoComplete="Duration  "
            />

             <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="Location"
              label="Location"
              type="Location"
              id="Location"
              onChange={(e)=>{
                getLocation(e.target.value)
              }}
              autoComplete="Location  "
            />

            
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="Price"
              label="Price"
              type="Price"
              id="Price"
              onChange={(e)=>{
                getPrice(e.target.value)
              }}
              autoComplete="Price  "
            />

            <Button
              fullWidth
              variant="contained"
              color="#ffd100"
              className={classes.submit}
              onClick={()=>{jobpost(title,category,duration,location,price)}}   
            >
              create a job
            </Button>
          </form> 
              </Paper>
           </Grid>
          </Grid>
        
        </Container>
      </main>
    </div>
  );
}

