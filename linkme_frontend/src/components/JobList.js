import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Title from '@material-ui/core/DialogTitle';
import { Button } from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Website Developer', 'Full-time', "More than 6 months", "Work in progress", 4.0),
  createData('React Developer', 'One-time', "Less than 1 months", "In review", 7.0),
  createData('Graphic Designer', 'Part-time', "3 to 6 months", "Pending", 7.0),
  createData('Back-end Developer', 'Full-time', "More than 6 months", "Work in progress", 0),
  createData('Digital Marketer', 'One-time', "Less than 1 months", "Pending", 9),
];

export default function JobList() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
          <Title> My Jobs </Title>   
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Job Title</TableCell>
            <TableCell align="right">Type</TableCell>
            <TableCell align="right">Duration&nbsp;</TableCell>
            <TableCell align="right">Job Status&nbsp;</TableCell>
            <TableCell align="right">Proposals&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.calories}</TableCell>
              <TableCell align="right">{row.fat}</TableCell>
              <TableCell align="right">{row.carbs}</TableCell>
              <TableCell align="right">{row.protein}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      &nbsp; &nbsp; &nbsp;      
      <Button variant="contained" onClick={()=>{window.location.href="/job-create"}}>
          Creat a job
      </Button>
       &nbsp; &nbsp; &nbsp; 
    </TableContainer>   
  );
}