import React from 'react';
// import IconConstruction from '../../images/construction.svg';
// import IconAgriculture from '../../images/agriculture.svg';
import IconManufacture from '../../../images/manufacture.svg';
import IconHospitality from '../../../images/hospitality.svg';
import IconRetail from '../../../images/retail.svg';
// import Icon3 from '../../images/svg1.svg';

import { ServicesContainer, ServicesCard , ServicesH1, ServicesH2, ServicesH3, 
    ServicesP, ServicesWrapper, ServicesIcon, } from './ServicesElements';

export const Services = () => {
    return (
        <ServicesContainer id="services">
            <ServicesH1>Available Categories</ServicesH1>
             <ServicesH3>You will find varities of job vacancies, Check them out in each categories.</ServicesH3>
            <ServicesWrapper>

            <ServicesCard>
                  <ServicesIcon src={IconHospitality}/> 
                  <ServicesH2>Web, Mobile and Software Dev</ServicesH2>
                  <ServicesP>Browse Vacancies</ServicesP>
                  
              </ServicesCard>
             
              <ServicesCard>
                  <ServicesIcon src={IconRetail}/>
                  <ServicesH2>Sales and Marketing</ServicesH2>
                  <ServicesP>Browse Vacancies</ServicesP>
                  
              </ServicesCard>

              <ServicesCard>
                  <ServicesIcon src={IconRetail}/>
                  <ServicesH2>Design and Creative  </ServicesH2>
                  <ServicesP>Browse Vaccancies</ServicesP>
                  
              </ServicesCard>

              <ServicesCard>
                  <ServicesIcon src={IconManufacture}/>
                  <ServicesH2>Others </ServicesH2>
                  <ServicesP>Browse Vacancies</ServicesP>
                   
              </ServicesCard>

              

            </ServicesWrapper>
     
        </ServicesContainer>          
    )
}

export default Services
