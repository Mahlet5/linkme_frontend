import React from 'react';
import {FaBars} from 'react-icons/fa'; 
import Button from '@material-ui/core/Button';
import {
    Nav, 
    NavbarContainer, 
    NavLogo, 
    MobileIcon, 
    NavMenu, 
    NavItem ,
    NavLinks,
    NavBtn,
    NavBtnLink
} from './NavbarElements';

const Navbar = ({ toggle }) => {
    return (
       <>
         <Nav>
             <NavbarContainer>
                 <NavLogo to='/'>LINKME</NavLogo>
                 <MobileIcon onClick={toggle}>
                     <FaBars />
                 </MobileIcon>
                 <NavMenu>
                      <NavItem>
                         <NavLinks to="home">Home</NavLinks>
                     </NavItem>
                     <NavItem>
                         <NavLinks to="about">About Us</NavLinks>
                     </NavItem>
                     {/* <NavItem>
                         <NavLinks to="discover">Jobs</NavLinks>
                     </NavItem> */}

                     <NavItem>
                         <NavLinks to="services">How it works</NavLinks>
                     </NavItem>

                     <NavItem>
                         <NavLinks  onClick={()=>{window.location.href="/signup"}}> Sign Up</NavLinks>
                     </NavItem>

                  
                 </NavMenu>
                
                 <NavBtn>
                     <NavBtnLink to="/signin" > Sign In</NavBtnLink>
                 </NavBtn>
             </NavbarContainer>
         </Nav>
       </>
    );
};

export default Navbar
