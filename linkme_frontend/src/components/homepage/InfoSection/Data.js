export const homeObjOne = {
    // id = 'about',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Linkme App',
    headline: 'RIGHT CANDIDATE FOR THE RIGHT JOB',
    description: 'Connects job seekers to suitable job opportunities, within the country. Our application has two platforms, website and mobile app.',
    buttonLabel: 'Get started',
    imgStart: false,
    img: require('../../../images/image1.svg'),
    alt: 'work',
    dark: true,
    primary: true,
    darkText: false
};

export const homeObjTwo = {
     // id = 'about',
     lightBg: false,
     lightText: true,
     lightTextDesc: true,
     topLine: '',
     headline: 'How it works',
     description: 'Connects job seekers to suitable job opportunities, within the country. Our application has two platforms, website and mobile app.',
     buttonLabel: 'Get started',
     imgStart: false,
     img: require('../../../images/image1.svg'),
     alt: 'work',
     dark: true,
     primary: true,
     darkText: false
};

export const homeObjThree = {
    // id = 'about',
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Linkme App',
    headline: 'RIGHT CANDIDATE FOR THE RIGHT JOB',
    description: 'Connects job seekers to suitable job opportunities, within the country. Our application has two platforms, website and mobile app.',
    buttonLabel: 'Get started',
    imgStart: true,
    img: require('../../../images/image1.svg'),
    alt: 'work',
    dark: false,
    primary: false,
    darkText: true
};