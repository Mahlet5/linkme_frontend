import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Button from '@material-ui/core/Button';
import MailIcon from '@material-ui/icons/Mail';
import Avatar from '@material-ui/core/Avatar';
import SearchBar from "material-ui-search-bar";

const drawerWidth = 0;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
    backgroundColor:'#ffd100',
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
    color:'#000000',
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
    color: '#000000',
    fontFamily: 'Roboto',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  Icon: {
    color: '#000000',
   
  },
  IconAvatar: {
    color: '#ffffff',
    backgroundColor: '#000000',
    height: '25px',
    width: '25px',

  },
  SearchBar: {
    backgroundColor: '#ffd100',
    color: '#000000',
    marginRight: '200px',
  }
 
}));

export default function Header() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
 
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
 
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>  
          <Typography component="h4" variant="h6" color="inherit" noWrap className={classes.title}>
            LINKME
          </Typography>

          <SearchBar className={classes.SearchBar} />

         
         <Button aria-controls="simple-menu"  aria-haspopup="true" onClick={()=>{window.location.href="/find-work"}}>FIND WORK</Button>  
  
         <Button aria-controls="simple-menu" aria-haspopup="true" onClick={()=>{window.location.href="/my-jobs"}}>MY JOBS</Button>
     
         <Button aria-controls="simple-menu" aria-haspopup="true" onClick={()=>{window.location.href="/report"}}>REPORTS</Button>
        
         <Button aria-controls="simple-menu" aria-haspopup="true" onClick={()=>{window.location.href="/message"}}>MESSAGES </Button>
       
         <IconButton color="inherit" className={classes.Icon}>
            <Badge badgeContent={0}>
              <NotificationsIcon />
            </Badge>
          </IconButton>

          <IconButton color="inherit" className={classes.Icon}>
           <Badge badgeContent={2}>
             <MailIcon />
           </Badge> 
        
           </IconButton>

           <IconButton>
              <Avatar className={classes.IconAvatar} />
           </IconButton>   
        
        </Toolbar>
      </AppBar>
  
    </div>
  );
}

