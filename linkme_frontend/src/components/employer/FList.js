import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
// import Star from '@material-ui/icons/Star';
import Typography from '@material-ui/core/Typography';
// import { red, yellow } from '@material-ui/core/colors';
// import FavoriteIcon from '@material-ui/icons/Favorite';
// import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import JobList from '../List';
import { Button } from '@material-ui/core';
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 1000,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    // backgroundColor: yellow[500],
    Color: '#ffffff',
  },
}));

export default function FList() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
//   const [users, setUsers] = React.useState([]);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

//   React.useEffect(() => {
//     axios.get('http://localhost:5000/users/employees').then(
//       (res)=>{
//         setUsers(res.data.user)
//         console.log(res)
//       }
//     )
//   }, []);

  // const clickAction = (id)=>{
  //   axios.get('http://localhost:5000/users/employees/'+id).then(
  //     (res)=>{
  //       window.alert("USER HAS BEEN ADDED!!")
  //     }
  //   )
  // }

  return (
    <div>
        <Card className={classes.root}>
        <CardHeader
          avatar={
            <Avatar aria-label="recipe" className={classes.avatar}>
              H
            </Avatar>
          }
          action={
            <IconButton aria-label="settings">
              <MoreVertIcon />
            </IconButton>
          }
          title= 'Robel Dereja'
          subheader= 'Full Stack Developer'
        /> 
        
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
          dddd
          </Typography>
        </CardContent>
        <CardActions disableSpacing>

         <Button variant="contained">
            Hire a freelancer
         </Button>
  
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </IconButton>

        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Skills</Typography>
            <Typography paragraph>
              Heat cup of the broth in a pot until simmering, add saffron and set aside for 10
              minutes.
            </Typography>
            <Typography paragraph>
              Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over medium-high
              heat. Add chicken, shrimp and chorizo, and cook, stirring occasionally until lightly
              browned, 6 to 8 minutes. Transfer shrimp to a large plate and set aside, leaving chicken
              and chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes, onion, salt and
              pepper, and cook, stirring often until thickened and fragrant, about 10 minutes. Add
              saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.
            </Typography>
            <Typography>
              Set aside off of the heat to let rest for 10 minutes, and then serve.
            </Typography>
          </CardContent>
        </Collapse>
      </Card>
      
    </div>
  );
}
