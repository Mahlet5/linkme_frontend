import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import WorkIcon from '@material-ui/icons/Work';
import { IconButton } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

export default function CategoriesList() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
       
        <ListSubheader component="div" id="nested-list-subheader">
            <IconButton>
              <WorkIcon />
            </IconButton>
          
           Freelanceer Categories 
        </ListSubheader>
      }
      className={classes.root}
    >
     
      <ListItem button>
        <ListItemText primary="Web Devlopment" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <ListItem button>  
        <ListItemText primary="Mobile Development" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <ListItem button>
        <ListItemText primary="Design" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <ListItem button>
        <ListItemText primary="Wrting" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <ListItem button>

     <ListItemText primary="Admin Support" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <ListItem button>
        <ListItemText primary="Customer Service" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <ListItem button>
        <ListItemText primary="Marketing" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <ListItem button>
        <ListItemText primary="Accounting" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <ListItem button>
        <ListItemText primary="See All Categories" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>


    
    </List>
  );
}
