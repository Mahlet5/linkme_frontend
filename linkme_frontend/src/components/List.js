import React from 'react';
import axios from 'axios';

export default class List extends React.Component {
    state = {
        forms: [],
    }
    componentDidMount() {
        axios.get("http://localhost:5000/users").then(res =>  {
            console.log(res.data);
            this.setState({ forms: res.data.user });
        });
    }
          
    render() {
     return ( 
     <ul>      
         {this.state.forms.map(form => (
         <li key={form.id}>{form.username} </li>
         ))}
     </ul>
     );
    }    
}