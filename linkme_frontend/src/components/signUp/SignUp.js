import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="">
        Linkme
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor:"#ffd100",
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor:"#ffd100",
  },
}));

function onSignUpF(username,firstname,middlename,lastname,email,password,skills,experiences){
  console.log("inside F")
  axios.post("http://localhost:5000/users/signup:Employee", {
    username: username,
    firstname: firstname,
    middlename: middlename,
    lastname: lastname,
    email: email,
    password: password,
    skills: skills,
    experiences: experiences,
  }).then(
    (res)=>{
    console.log("hell0")
      window.location="/find-work"

    }

  ).catch(
    (err)=>{
       console.log(err)
    }
  )
}


function onSignUpE(username,firstname,middlename,lastname, email,password,interest){

  axios.post("http://localhost:5000/users/signup:Employer", {
    username: username,
    firstname: firstname,
    middlename: middlename,
    lastname: lastname,
    email: email,
    password: password,
    interest: interest,
  }).then(
    (res)=>{
      window.location="/find-freelancer"
    }

  ).catch(
    (err)=>{
       console.log(err)
    }
  )
}

export default function SignUp() {
  const classes = useStyles();

  const [username, getUsername] = useState(0);
  const [firstname, getFirstname] = useState(0);
  const [middlename, getMiddlename] = useState(0);
  const [lastname, getLastname] = useState(0);
  const [email, getEmail] = useState(0);  
  const [password, getPassword] = useState(0);

  const [skills, getSkills] = useState(0);
  const [experiences, getExperiences] = useState(0);
  const [interest, getInterest] = useState(0);

  const [clickedContent, setClickedContent] = React.useState();
  const [isClicked, setClicked] = React.useState(false);

  function HireContent(){
    return(
      <div>
          <TextField
                variant="outlined"
                required
                fullWidth
                id="interest"
                onChange={(e)=>{
                  getInterest(e.target.value)
                }}
                label="Interests"
                name="interest"
                autoComplete="interest"
              />
                  &nbsp; &nbsp; &nbsp; 
               &nbsp; &nbsp; &nbsp;
              

              <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            // href="./employeeForm"
            className={classes.submit}
            onClick={()=>{onSignUpE(username,firstname,middlename,lastname,email,password,skills,interest)}}   
          >
            Create My Account for an employee
          </Button>
          </div>
    );
  }
  function WorkContent(){
    return(
      <div>
        <TextField
                variant="outlined"
                required
                fullWidth
                id="skills"
                onChange={(e)=>{
                  getSkills(e.target.value)
                }}
                label="Skills"
                name="skills"
                autoComplete="skills"
              />
                &nbsp; &nbsp; &nbsp; 
                &nbsp; &nbsp; &nbsp; 
                <TextField
                variant="outlined"
                required
                fullWidth
                id="experiences"
                onChange={(e)=>{
                  getExperiences(e.target.value)
                }}
                label="Experiences"
                name="experiences"
                autoComplete="experiences"
              />

            <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            // href="./employeeForm"
            className={classes.submit}
            onClick={()=>{onSignUpF(username,firstname,middlename,lastname,email,password,skills,experiences)}}   
          >
            Create My Account for a freelancer
          </Button>
            
      </div>
    );
  }

  const hireButtonClicked=()=>{
    setClicked(true);
    setClickedContent(<HireContent/>)
  }
  const workButtonClicked=()=>{
    setClicked(true);
    setClickedContent(<WorkContent/>)
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="username"
                name="username"
                variant="outlined"
                required
                fullWidth
                id="username"
                onChange={(e)=>{
                  getUsername(e.target.value)
                }}
                label="User Name"
                autoFocus
              />
            </Grid>
            
            <Grid item xs={12} sm={6}>
            <TextField
                autoComplete="firstname"
                name="firstname"
                variant="outlined"
                required
                fullWidth
                id="firstname"
                onChange={(e)=>{
                  getFirstname(e.target.value)
                }}
                label="First Name"
                autoFocus
              />
            </Grid>

            <Grid item xs={12} sm={6}>
            <TextField
                variant="outlined"
                required
                fullWidth
                id="middlename"
                onChange={(e)=>{
                  getMiddlename(e.target.value)
                }}
                
                label="Middle Name"
                name="middlename"
                autoComplete="middlename"
              />  
            </Grid>
            
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastname"
                onChange={(e)=>{
                  getLastname(e.target.value)
                }}
                
                label="Last Name"
                name="lastame"
                autoComplete="lastname"
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                onChange={(e)=>{
                  getEmail(e.target.value)
                }}
                label="Email Address"
                name="email"
                autoComplete="email"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={(e)=>{
                  getPassword(e.target.value)
                }}
                autoComplete="password"
              />
            </Grid>
           
            {!isClicked && <Button variant="contained" onClick={hireButtonClicked}>
              Hire for a Project  
            </Button>}
         

            &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp; 
         

            {!isClicked && <Button  variant="contained" color="#ffd100" onClick={workButtonClicked}>
               Work as a Freelancer
            </Button>}
           {isClicked&& clickedContent}
            
          </Grid>

          {/* <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            // href="./employeeForm"
            className={classes.submit}
            // onClick={()=>{onSignUp(username,firstname,lastname,email,password,skills,experiences,interest)}}   
          >
            Next
          </Button> */}
          <Grid container justify="flex-end">
            <Grid item>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}