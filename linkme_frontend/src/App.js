import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './pages';
import SigninPage from './pages/signin';
import SignupPage from './pages/signup';
import EmployeePage from './pages/find-work';
import EmployerPage from './pages/find-freelancer';
import Report from './pages/report';
import Jobs from './pages/jobs';
import Message from './pages/message';
import MyJobs from './pages/my-jobs';
import FreelancerProposal from './pages/freelancer-proposal';
import Admin from './pages/admin';
import JobCreate from './pages/job-create';

import axios from 'axios';

const accessToken = '';

axios.interceptors.request.use(
  config => {
    config.headers.authorization = `Bearer ${accessToken}`;
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

class App extends React.Component{

  // componentDidMount(){
  //   console.log("here")
  // }
  render(){   
    return (
      <Router>
        <Switch>
          <Route path="/" component={Home} exact /> 
          <Route path="/signin" component={SigninPage} exact /> 
          <Route path="/signup" component={SignupPage} exact /> 
          <Route path="/find-work" component={EmployeePage} exact /> 
          <Route path="/find-freelancer" component={EmployerPage} exact /> 
          <Route path="/report" component={Report} exact /> 
          <Route path="/jobs" component={Jobs} exact /> 
          <Route path="/message" component={Message} exact />
          <Route path="/my-jobs" component={MyJobs} exact />  
          <Route path="/freelancer-proposal" component={FreelancerProposal} exact /> 
          <Route path="/job-create" component={JobCreate} exact /> 
          <Route path="/adminpage" component={Admin} exact /> 


        </Switch>

      {/* <Home />
      <Signin /> */}
      
      </Router>
    );
    
  }
}

export default App;
