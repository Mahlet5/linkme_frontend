import React, {useState} from 'react';
import Navbar from '../components/homepage/Navbar';
import Sidebar from '../components/homepage/Sidebar';
import InfoSection from '../components/homepage/InfoSection';
import { homeObjOne } from '../components/homepage/InfoSection/Data';
import Services from '../components/homepage/Services';
import Footer from '../components/homepage/Footer';

//import StickyFooter from '../components/StickyFooter/StickyFooter';

export const Home = () => {

    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => {
        setIsOpen(!isOpen)
    };


    return (
      <>
        <Sidebar isOpen={isOpen} toggle={toggle} />
        <Navbar toggle={toggle} /> 

        <InfoSection {...homeObjOne}/> 
        <Services />
      {/* <StickyFooter />  */}
         <Footer />  
        </>   
    );
};

export default Home 