import React from 'react';
import Proposal from '../components/Proposal';
import Header from '../components/employee/Header';
import Footer from '../components/employee/Footer';

export const FreelancerProposal= () => {
    return (
        <>
      <Header />
     <Proposal />  
     <Footer />

        </>   
    )
}
export default FreelancerProposal;