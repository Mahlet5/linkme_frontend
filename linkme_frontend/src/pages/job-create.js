import React from 'react';
import Header from '../components/employee/Header';
import Footer from '../components/employee/Footer';
import JobCreate from '../components/JobCreate';


export const JobC= () => {
    return (
        <>
        <Header />
        <JobCreate />
        <Footer />
  
        </>   
    )
}
export default JobC;