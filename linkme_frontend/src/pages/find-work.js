import React from 'react';
import Dashboard from '../components/employee/Dashboard';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Header from '../components/employee/Header';
import Footer from '../components/employee/Footer';
import JobList from '../components/employee/JobList';
import CategorieList from '../components/employee/CategorieList';



export const EmployeePage = () => {
    return (
        <>    
           
            <Header />
           <Dashboard />    
            <Footer />

         </>   
    )
}

export default EmployeePage;
